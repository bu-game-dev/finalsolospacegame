﻿using System;
using UnityEngine;

namespace Singleton
{


    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {

        private static bool m_ShuttingDown = false;
        private static object m_Lock = new object();
        private static T m_Instance;


        public static T Instance
        {
            get
            {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed. Returning null.");
                    return null;
                }

                lock (m_Lock)
                {
                    if (m_Instance == null)
                    {

                        m_Instance = (T) FindObjectOfType(typeof(T));


                        if (m_Instance == null)
                        {

                            var singletonObject = new GameObject();
                            m_Instance = singletonObject.AddComponent<T>();
                            singletonObject.name = typeof(T).ToString() + " (Singleton)";

                        }
                    }

                    return m_Instance;
                }
            }
        }

        protected void Awake()
        {
            if (Instance!= null && Instance!= this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }


        private void OnApplicationQuit()
        {
            m_ShuttingDown = true;
        }


        private void OnDestroy()
        {
            m_ShuttingDown = true;
        }
    }
}