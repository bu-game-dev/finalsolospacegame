﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private GameObject[] waypoint ;
        [SerializeField] private float enemyShipSpeed = 3;
        private float chasingThresholdDistance = 1.0f;
        private int curant;
        void FixedUpdate()
        {
            MoveToPlayer();
        }
        private void MoveToPlayer()
        {
            if (Vector3.Distance(waypoint[curant].transform.position,transform.position)<1)
            {
                curant++;
                if (curant >= waypoint.Length)
                {
                    curant = 0;
                }
            }

            transform.position = Vector3.MoveTowards(transform.position, waypoint[curant].transform.position,Time.deltaTime*enemyShipSpeed);
        }
    }
}