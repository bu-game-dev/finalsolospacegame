using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        private float firecount;
        [SerializeField] private double          firerate; 
        [SerializeField] private float           destroyEnemySound = 0.3f;
        [SerializeField] private PlayerSpaceship playerja;
        [SerializeField] private AudioClip       destroyEnemy;
        [SerializeField] private Transform       gunMod01;
        [SerializeField] private Transform       gunMod02;
        
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            } 
            
           
            Explode();
           
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
            if (!(Camera.main is null))
                AudioSource.PlayClipAtPoint(destroyEnemy, Camera.main.transform.position, destroyEnemySound);
            OnExploded?.Invoke();  
           
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.MoveIng();
            if (ScoreManager.Instance.countlevel == 2) 
            {
                var bullet1 = Instantiate(defaultBullet, gunMod01.position, Quaternion.identity);
                bullet1.Movemodbullet();
                var bullet2 = Instantiate(defaultBullet, gunMod02.position, Quaternion.identity);
                bullet2.Movemodbullet();
            }

        }

        private void Update()
        {
            firecount += Time.deltaTime;
            if (playerja.Hp>=0)
            {
                if (firecount >= firerate)
                {
                    Fire();
                    firecount = 0;
                }

            }
            if (Hp<=0)
            {
                ScoreManager.Instance.countlevel = 2;
            }

            
           
        }
    }
}