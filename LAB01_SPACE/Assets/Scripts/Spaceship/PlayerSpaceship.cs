using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip destroyPlayer;
        [SerializeField] private float destroySound = 0.3f;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(destroyPlayer,Camera.main.transform.position,destroySound);
            OnExploded?.Invoke();
          
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Star"))
            {
                Destroy(other.gameObject);
            }
        }
    }
}