﻿using Singleton;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SocialPlatforms.Impl;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;

        [SerializeField] private TextMeshProUGUI finalScoreText;
        private GameManager gameManager;
        private int playerScore;
        public int countlevel =1;
        
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            
        }

        public void SetScore(int score)
        { 
            playerScore = score;
            scoreText.text = $"Score : {score}";
           
        }

        public void SetScoreZero()
        {
             playerScore = 0;
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }
        
        private void OnRestarted()
        {
           
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            
        }

        public void OnPlayerDie()
        {
            finalScoreText.text = $"You Die Noob Endgame ";
        }
        public void OnShowFinalScoer()
        {
            finalScoreText.text = $"Player Score : {playerScore} Endgame ";
           
        }
        public void OFFShowFinalScoer()
        {
            finalScoreText.gameObject.SetActive(false);
           
        }
        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


