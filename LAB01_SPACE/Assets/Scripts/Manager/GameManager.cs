﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        //[SerializeField] private AudioClip playerDesTroy;
        //[SerializeField] private AudioClip enemyDesTroy;
        [SerializeField] private Button          startButton;
        [SerializeField] private Button          startButtonlevel2;
        [SerializeField] private RectTransform   dialog;
        [SerializeField] private RectTransform   dialogScene2;
       
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship  enemySpaceship;
        [SerializeField] private ScoreManager    scoreManager;
        
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        public int countlevel=1;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            startButtonlevel2.onClick.AddListener(OnStartButtonClicked);
            
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            dialogScene2.gameObject.SetActive(false);
            StartGame();
           
            
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
            ScoreManager.Instance.OnPlayerDie();
            
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
           
        }

        private void OnEnemySpaceshipExploded()
        {
            if (ScoreManager.Instance.countlevel == 2)
            { 
                ScoreManager.Instance.SetScore(2);
                ScoreManager.Instance.OnShowFinalScoer();
                Restart();
                

                

            }

            if (ScoreManager.Instance.countlevel == 1)
            {
                Restart();
                dialog.gameObject.SetActive(false);
                dialogScene2.gameObject.SetActive(true);
                ScoreManager.Instance.SetScore(1);
                ScoreManager.Instance.countlevel = 2;
               
            }



        }

        private void Restart()
        {
            DestroyCount();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
            if (ScoreManager.Instance.countlevel == 2)
            {
                 ScoreManager.Instance.SetScoreZero();
            }
           
        }

        private void DestroyCount()
        {
            var remaingEnemys = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remaingEnemys)
            {
                Destroy(enemy);
            }
            var remaingplayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remaingplayer)
            {
                Destroy(player);
            }
        }

    }
}
